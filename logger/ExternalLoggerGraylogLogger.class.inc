<?php
/**
 * @file
 * Handles logging watchdog entry logs towards a GELF compatible backend.
 */

// @TODO: Why does this class not get autoloaded?
require_once 'ExternalLoggerLogger.class.inc';

/**
 * Class ExternalLoggerGraylogLogger.
 */
class ExternalLoggerGraylogLogger extends ExternalLoggerLogger {

  /**
   * The \Gelf\Message wrapper.
   *
   * @var Gelf\Message wrapper for the log_entry.
   */
  protected $wrapper = NULL;

  /**
   * Constructor of the Graylog outgoing logger.
   *
   * @param \Gelf\Publisher $publisher
   *   Publisher used to wrap GELF transports.
   * @param \Gelf\Message $wrapper
   *   Wrapper to send to the GELF backend.
   */
  public function __construct(\Gelf\Publisher $publisher, \Gelf\Message $wrapper) {
    parent::__construct($publisher);
    $this->wrapper = $wrapper;
  }

  /**
   * Convert the provided log_entry information into a message wrapper.
   *
   * @param array $log_entry
   *    Log entry provided by watchdog.
   */
  public function log(array $log_entry) {
    // If the wrapper is not specified do not run.
    if ($this->wrapper == NULL) {
      return;
    }

    $user = $log_entry['user'];
    $env = (module_exists('kw_environment')) ? kw_environment() : variable_get(EXTERNAL_ENVIRONMENT);
    $psr = $this->wrapper->logLevelToPsr($log_entry['severity']);
    $server_address = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : NULL;

    $name = "Anonymous";
    $email = "Anonymous@Nothing.mail";
    if ($log_entry['uid'] !== 0) {
      $name = $user->name;
      $email = $user->mail;
    }

    // Wrap the log_entry for GELF transport.
    $this->wrapper->setFacility("tamtam-external-logger")
      ->setHost($GLOBALS['base_url'] . '|' . $server_address)
      ->setShortMessage($this->buildMessage($log_entry, $server_address, $psr))
      ->setTimestamp($log_entry['timestamp'])
      ->setLevel($log_entry['severity'])
      ->setAdditional('severity_label', drupal_strtoupper($psr))
      ->setAdditional('type', $log_entry['type'])
      ->setAdditional('user', $user->uid . ',' . $name . ',' . $email)
      ->setAdditional('request_uri', $log_entry['request_uri'])
      ->setAdditional('referrer', $log_entry['referer'])
      ->setAdditional('request_ip', $log_entry['ip'])
      ->setAdditional('environment', $env);

    $this->publisher->publish($this->wrapper);
  }

  /**
   * Build a human readable message string for the GELF message field.
   *
   * @param array $log_entry
   *   Log entry provided by watchdog.
   * @param string $server_address
   *   Server address of the server sending the message.
   * @param string $psr
   *   The PSR level as label.
   *
   * @return string
   *   Returns the human readable message.
   */
  private function buildMessage(array $log_entry, $server_address, $psr) {
    $message = "\nSite:         @base_url";
    $message .= "\nSeverity:     (@severity) @severity_desc";
    $message .= "\nTimestamp:    @timestamp";
    $message .= "\nType:         @type";
    $message .= "\nIP Address:   @ip";
    $message .= "\nRequest URI:  @request_uri";
    $message .= "\nReferrer URI: @referer_uri";
    $message .= "\nUser:         (@uid) @name";
    $message .= "\nLink:         @link";
    $message .= "\nMessage:      \n\n@message";

    $phold_message = $log_entry['message'];
    $variables = $log_entry['variables'];

    $short_message = ($variables == NULL) ? $phold_message : t($phold_message, $variables);
    $short_message = strip_tags($short_message);

    $name = "Anonymous";
    if ($log_entry['uid'] !== 0) {
      $name = $log_entry['user']->name;
    }

    $message = t($message, array(
      '@base_url'      => $server_address,
      '@severity'      => $log_entry['severity'],
      '@severity_desc' => $psr,
      '@timestamp'     => format_date($log_entry['timestamp']),
      '@type'          => $log_entry['type'],
      '@ip'            => $log_entry['ip'],
      '@request_uri'   => $log_entry['request_uri'],
      '@referer_uri'   => $log_entry['referer'],
      '@uid'           => $log_entry['uid'],
      '@name'          => $name,
      '@link'          => strip_tags($log_entry['link']),
      '@message'       => $short_message,
    ));

    return $message;
  }

}
