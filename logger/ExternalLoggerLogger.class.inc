<?php

/**
 * @file
 * Abstract Logger class.
 */

/**
 * Class ExternalLoggerLogger.
 */
abstract class ExternalLoggerLogger implements ExternalLoggerLoggerInterface {
  protected $publisher;

  /**
   * Constructor for the abstract logger.
   *
   * @param mixed $publisher
   *   Publisher to wrap transports.
   */
  public function __construct($publisher) {
    $this->publisher = $publisher;
  }

}
