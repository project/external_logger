<?php
/**
 * @file
 * Interface for the Abstract Logger class.
 */

/**
 * Interface LoggerInterface.
 */
interface ExternalLoggerLoggerInterface {

  /**
   * Log an watchdog entry.
   *
   * @param array $log_entry
   *   The log_entry containing information.
   */
  public function log(array $log_entry);

}
