CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION (EXTERNAL LOGGER) - WIP
-----------------------------
TamTam External Logger module for Drupal is a module that utilizes
hook_watchdog() to catch all log events on a Drupal based site.
And send the events to a compatible backend. The module is designed
to center log messages of multiple environments and sites in a single
location. All environments can be grouped to form a practical overview. Saving
the developer the time of looking up multiple logs and/or multiple machines.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/neodork/2553265


REQUIREMENTS
------------
This module requires the following modules:
 * Composer Manager (https://www.drupal.org/project/composer_manager)
 * Libraries (https://www.drupal.org/project/libraries)


Required Modules
------------------
- Composer Manager
- Libraries


RECOMMENDED MODULES
------------------
- kw_environment


INSTALLATION
------------
 * Install External Logger as you would normally install a Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


 * Composer Manager should install all the composer dependencies.

 * Go to: /admin/config/system/external_logger and configure your server
   settings

 * (Optional) Run "drush external_logger test-message" to test the installation.


CONFIGURATION
-------------
 * Configure module settings in Configuration � System � External Logger
 (/admin/config/system/external_logger):

    - Site Environment

    (Global) The environment you're currently running. E.g: Production,
     Development, Acceptance, etc...


    - External Logger Transport Name

    Human readable name of the transport. Given by the administrator.


    - IP

    The IP/DNS of the external server.


    - Transport Type

    The type of transport to configure.


    - Port

    The port of the external server.


    - Basic Auth Username

    The Basic Auth Username.


    - Basic Auth Password

    The Basic Auth password.


    - Use SSL

    Checkbox use SSL or not? (checked = yes)


    - Check self signed

     Check self signed certificate yes/no?


     - Verify peer

     Verify the peer yes/no?


  * Fields send to the external server:
      - facility

      The facility providing the external server messages.
      (in this case: tamtam-external-logger)


      - host

      The host and IP in the following format: <Host>|<IP>


      - short Message

      A short human readable message explaining the event.


      - timestamp

      Timestamp of when the event occurred.


      - level

      The level of severity of the event (integer).


      - severity_label

      A human readable representation of the severity level.


      - type

      The type (origin) of the error.


      - user

      <ID>,<Username>,<E-mail> The user which triggered the log entry.

      - request_uri

      The request URI for the page the event happened in.


      - referer

      The page that referred the user to the page where the event occurred.


      - request_ip

      The IP address where the request for the page came from.


      - environment

      The environment string the site is currently running on.

TROUBLESHOOTING
---------------
 * The module is not sending message for some unknown reason:


   - run command "drush el tm" and follow the instructions


FAQ
---
NONE


MAINTAINERS
-----------
Current maintainers:
 * Lou van der Laarse (Neodork) - https://www.drupal.org/u/neodork


This project has been sponsored by:
 * TO BE ANNOUNCED
