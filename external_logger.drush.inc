<?php
/**
 * @file
 * Drush hook implementations for the External Logger module.
 */

/**
 * Implements hook_drush_command().
 */
function external_logger_drush_command() {

  $items['external_logger'] = array(
    'description' => 'Access the External Logger drush commands. drush external_logging help for more information.',
    'aliases'     => array('el'),
    'callback'    => 'drush_external_command',
    'arguments'   => array(
      'command' => 'The sub-command for External Logger to run.',
    ),
  );

  return $items;
}

/**
 * Executes a External Logger command.
 *
 * @param string $command
 *   Command to execute.
 */
function drush_external_command($command = 'help') {
  if ($command == 'help' || $command == 'h') {
    help();
  }

  if ($command == 'test-message' || $command == 'tm') {
    test_message();
  }

}

/**
 * Help function to print out all commands.
 */
function help() {
  drupal_set_message(PHP_EOL);
  drupal_set_message(t('help | h => Show the help(this) interface.'));
  drupal_set_message(t('test-message | tm => Send out a test message to the configured external logging server.'), 'status');
  drupal_set_message(PHP_EOL);
}

/**
 * Test message function to send a message over all configured transports.
 */
function test_message() {
  // Prevent hooks from firing without gelf.
  $composer_path = variable_get('composer_manager_vendor_dir');
  $composer_exists = file_exists($composer_path) != NULL;
  $gelf_exists = file_exists($composer_path . '/graylog2/gelf-php');

  if (!$composer_exists || !$gelf_exists) {
    if (drush_confirm('GELF-PHP Library not found! External Logger is trying to install GELF!' . PHP_EOL .
      'Are you sure you want External Logger to try and install the GELF-PHP library?')) {
      drush_composer_manager();
    }
    return;
  }

  $composer_path = variable_get('composer_manager_vendor_dir');
  $composer = file_exists($composer_path) != null;

  $config = variable_get(EXTERNAL_TRANSPORTS);
  $publisher = new Gelf\Publisher();
  // Add every transport defined in the config to the publisher.
  external_logger_build_transports($config, $publisher);

  global $user;
  $user->name = 'drush';
  $user->mail = 'drush@gmail.com';

  $log_entry = array(
    'message'     => 'Testing connection with the External server(s)! Drush external_logger test-message',
    'timestamp'   => microtime(TRUE),
    'severity'    => 6,
    'user'        => $user,
    'request_uri' => 'test/uri',
    'referrer'     => 'Drush external-logger test-message command',
    'request_ip'  => '0.0.0.0',
    'environment' => 'test message',
  );

  $external_logger = new ExternalLoggerGraylogLogger($publisher, new Gelf\Message());

  try {
    $external_logger->log($log_entry);
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage());
    return;
  }

  drupal_set_message(t('Succesfully send GELF message'));
}
