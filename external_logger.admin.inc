<?php
/**
 * @file
 * Administrative settings for the Composer Manager module.
 */

const ADD_TRANSPORT_HOOK = 'external_logger_config_form_add_transport';
const REMOVE_TRANSPORT_HOOK = 'external_logger_config_form_remove_transport';
const RESET_TRANSPORT_HOOK = 'external_logger_config_form_reset_transport';

/**
 * Build the config form for External Logger.
 *
 * @return array
 *   The form to return
 */
function external_logger_config_form() {
  $transport_config = variable_get(EXTERNAL_TRANSPORTS);

  // If the transport config does not exist insert defaults.
  if (empty($transport_config)) {
    $transport_config = array(external_logger_get_default_transport_config());
  }

  $form = array(
    '#tree' => TRUE,
  );

  $form['global'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Global Settings',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );

  $form['global'][EXTERNAL_ENVIRONMENT] = array(
    '#type'          => 'textfield',
    '#title'         => t('Site Environment'),
    '#default_value' => variable_get(EXTERNAL_ENVIRONMENT, 'development'),
    '#size'          => 15,
    '#maxlength'     => 28,
    '#description'   => t("The environment variable for non-kraftwagen projects."),
    '#required'      => TRUE,
  );

  // Generate fieldsets for every transport located in the configuration.
  $i = 0;
  foreach ($transport_config as $transport) {
    $form[] = external_logger_generate_transporter_form($i, $transport);
    $i++;
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Save Configuration',
  );

  $form['add_transport'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add transport'),
    '#submit' => array(ADD_TRANSPORT_HOOK),
  );

  $form['remove_transport'] = array(
    '#type'   => 'submit',
    '#value'  => t('Remove transport'),
    '#submit' => array(REMOVE_TRANSPORT_HOOK),
  );

  $form['reset_transport'] = array(
    '#type'       => 'submit',
    '#value'      => t('Reset transport'),
    '#submit'     => array(RESET_TRANSPORT_HOOK),
    '#attributes' => array(
      'onclick' => 'if(!confirm("This will reset ALL your transports and return the
      settings to their defaults!\n\nDo you really want to DELETE ALL TRANSPORTS?")){return false;}',
    ),
  );

  return $form;
}

/**
 * Generate the form elements for a single transport.
 *
 * @param int $i
 *   Index of the transport.
 * @param array $transport
 *   Configuration array for the transport.
 *
 * @return array
 *   The form elements for a single transport.
 */
function external_logger_generate_transporter_form($i, array $transport) {

  $form[$i] = array(
    '#type'        => 'fieldset',
    '#title'       => $transport[EXTERNAL_NAME],
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );

  $form[$i][EXTERNAL_NAME] = array(
    '#type'          => 'textfield',
    '#title'         => t('External Logger Transport Name'),
    '#default_value' => $transport[EXTERNAL_NAME],
    '#size'          => 15,
    '#maxlength'     => 56,
    '#description'   => t("The IP/DNS of the external logging server."),
    '#required'      => TRUE,
  );

  $form[$i][EXTERNAL_TYPE] = array(
    '#type'          => 'select',
    '#title'         => t('Transport Type'),
    '#options'       => array(
      0 => t('HTTP'),
      1 => t('UDP'),
    ),
    '#default_value' => $transport[EXTERNAL_TYPE],
    '#description'   => t('Select the type of transport.'),
  );

  $form[$i][EXTERNAL_IP] = array(
    '#type'          => 'textfield',
    '#title'         => t('External logging server IP'),
    '#default_value' => $transport[EXTERNAL_IP],
    '#size'          => 15,
    '#maxlength'     => 56,
    '#description'   => t("The IP/DNS of the external logging server."),
    '#required'      => TRUE,
  );

  $form[$i][EXTERNAL_PORT] = array(
    '#type'          => 'textfield',
    '#title'         => t('External logging Port'),
    '#default_value' => $transport[EXTERNAL_PORT],
    '#size'          => 15,
    '#maxlength'     => 7,
    '#description'   => t("The port of the external logging server."),
    '#required'      => TRUE,
  );

  $form[$i]['basic_auth'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Basic Auth'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#parents'     => array(
      $i,
      $i,
    ),
    '#access'      => !$transport[EXTERNAL_TYPE],
  );

  $form[$i]['basic_auth'][EXTERNAL_USERNAME] = array(
    '#type'          => 'textfield',
    '#title'         => t('Basic Auth Username'),
    '#default_value' => $transport[EXTERNAL_USERNAME],
    '#size'          => 15,
    '#maxlength'     => 56,
    '#description'   => t("Basic Auth username variable for external logging server authentication."),
  );

  $form[$i]['basic_auth'][EXTERNAL_PASSWORD] = array(
    '#type'        => 'password',
    '#title'       => t('Basic Auth Password'),
    '#size'        => 15,
    '#maxlength'   => 28,
    '#description' => t("Basic Auth password variable for external logging server authentication."),
  );

  if (!empty($transport[EXTERNAL_PASSWORD])) {
    $form[$i]['basic_auth']['password_label'] = array(
      '#markup' => '<i>The basic auth password is set!</i>',
    );
  }

  // SSL layer.
  $form[$i]['ssl'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('SSL options'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#parents'     => array(
      $i,
      $i,
    ),
    '#access'      => !$transport[EXTERNAL_TYPE],
  );

  $form[$i]['ssl'][EXTERNAL_USE_SSL] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use SSL?'),
    '#default_value' => $transport[EXTERNAL_USE_SSL],

  );

  $form[$i]['ssl'][EXTERNAL_SSC] = array(
    '#type'          => 'select',
    '#title'         => t('Check self signed'),
    '#options'       => array(
      0 => 'Yes',
      1 => 'No',
    ),
    '#default_value' => $transport[EXTERNAL_SSC],
    '#description'   => t('Check the certificate to be self signed or not?'),
  );

  $form[$i]['ssl'][EXTERNAL_VERIFY_PEER] = array(
    '#type'          => 'select',
    '#title'         => t('Verify peer?'),
    '#options'       => array(
      0 => 'Yes',
      1 => 'No',
    ),
    '#default_value' => $transport[EXTERNAL_VERIFY_PEER],
    '#description'   => t('Verify the peer?'),
  );
  return $form;
}

/**
 * Validate the input form_state returned by the config form.
 *
 * @param array $form
 *   Form sent to the frontend.
 * @param array $form_state
 *   Form_state returned from frontend.
 */
function external_logger_config_form_validate(array $form, array &$form_state) {
  $i = 0;
  foreach ($form_state['values'] as $values) {
    // If not array, continue.
    if (!is_array($values)) {
      continue;
    }
    // If the key equals the global variable, continue.
    if (key($values) === EXTERNAL_ENVIRONMENT) {
      continue;
    }

    $values = $values[$i];

    $name = $values[EXTERNAL_NAME];
    $type = $values[EXTERNAL_TYPE];
    $ip = $values[EXTERNAL_IP];
    $port = $values[EXTERNAL_PORT];
    $username = $values[EXTERNAL_USERNAME];
    $use_ssl = $values[EXTERNAL_USE_SSL];
    $check_ssc = intval($values[EXTERNAL_SSC]);
    $verify_peer = intval($values[EXTERNAL_VERIFY_PEER]);

    if ($type != 0 && $type != 1) {
      form_set_error(EXTERNAL_TYPE,
        t('@name: Invalid TYPE!',
          array('@name' => $name)
        )
      );
    }

    if (!is_string($name)) {
      form_set_error(EXTERNAL_NAME,
        t('@name: The transport name must be a string!',
          array('@name' => $name)
        )
      );
    }

    if (strpos($name, ' ') > 0 || strpos($name, ',') > 0 || strpos($name, "'") !== FALSE) {
      form_set_error(EXTERNAL_NAME,
        t('@name: Transport name cannot contain: spaces , or @apostrophe',
          array(
            '@name'       => $name,
            '@apostrophe' => chr(39),
          )
        )
      );
    }

    if ($ip < 0) {
      form_set_error(EXTERNAL_IP,
        t('@name: Negative IP&apos;s are cool, but not today.. Today we&apos;re boring',
          array('@name' => $name)
        )
      );
    }

    if (strpos($ip, ' ') > 0 || strpos($ip, "'") !== FALSE) {
      form_set_error(EXTERNAL_IP,
        t('@name: IP cannot contain: spaces . or @apostrophe',
          array(
            '@name'       => $name,
            '@apostrophe' => chr(39),
          )
        )
      );
    }

    if ($port <= 0) {
      form_set_error(EXTERNAL_PORT,
        t('@name: Port number must be positive!',
          array('@name' => $name)
        )
      );
    }

    if (!is_string($username)) {
      form_set_error(EXTERNAL_USERNAME,
        t('@name: The username must be a string!',
          array('@name' => $name)
        )
      );
    }

    if ($use_ssl != 0 && $use_ssl != 1) {
      form_set_error(EXTERNAL_USE_SSL,
        t('@name: The SSL value must be true or false!',
          array('@name' => $name)
        )
      );
    }

    if ($check_ssc != 0 && $check_ssc != 1) {
      form_set_error(EXTERNAL_SSC,
        t('@name: The SSC value must be true or false!',
          array('@name' => $name)
        )
      );
    }

    if ($verify_peer != 0 && $verify_peer != 1) {
      form_set_error(EXTERNAL_VERIFY_PEER,
        t('@name: The verify peer value must be true or false!',
          array('@name' => $name)
        )
      );
    }

    $i++;
  }

  // Check global config for the environment variable.
  $env = $form_state['values']['global'][EXTERNAL_ENVIRONMENT];

  if (!is_string($env)) {
    form_set_error(EXTERNAL_ENVIRONMENT, t('Global: The environment variable must be a string!'));
  }

  if (strpos($env, ' ') > 0 || strpos($env, "'") !== FALSE) {
    form_set_error(EXTERNAL_ENVIRONMENT,
      t('Global: The Environment cannot contain: spaces or @apostrophe',
        array(
          '@apostrophe' => chr(39),
        )
      )
    );
  }
}

/**
 * Check submission of the config form. Only submit changed transports.
 *
 * @param array $form
 *   Form send to the frontend.
 * @param array $form_state
 *   Form state returned from the frontend.
 */
function external_logger_config_form_submit(array $form, array &$form_state) {
  $transport_config = variable_get(EXTERNAL_TRANSPORTS);

  $i = 0;
  foreach ($form_state['values'] as $values) {
    // If not array, continue.
    if (!is_array($values)) {
      continue;
    }
    // If the key equals the global variable, continue.
    if (key($values) === EXTERNAL_ENVIRONMENT) {
      continue;
    }

    $vars = $form[$i][$i];
    $values = $values[$i];

    if ($values[EXTERNAL_NAME] <> $vars[EXTERNAL_NAME]['#default_value']
      || $values[EXTERNAL_TYPE] <> $vars[EXTERNAL_TYPE]['#default_value']
      || $values[EXTERNAL_IP] <> $vars[EXTERNAL_IP]['#default_value']
      || $values[EXTERNAL_PORT] <> $vars[EXTERNAL_PORT]['#default_value']
      || $values[EXTERNAL_USERNAME] <> $vars['basic_auth'][EXTERNAL_USERNAME]['#default_value']
      || $values[EXTERNAL_USE_SSL] <> $vars['ssl'][EXTERNAL_USE_SSL]['#default_value']
      || $values[EXTERNAL_SSC] <> $vars['ssl'][EXTERNAL_SSC]['#default_value']
      || $values[EXTERNAL_VERIFY_PEER] <> $vars['ssl'][EXTERNAL_VERIFY_PEER]['#default_value']
      || !empty($values[EXTERNAL_PASSWORD])
    ) {

      if (empty($values[EXTERNAL_PASSWORD]) && !empty($transport_config[$i][EXTERNAL_PASSWORD])) {
        $values[EXTERNAL_PASSWORD] = $transport_config[$i][EXTERNAL_PASSWORD];
      }

      $transport_config[$i] = $values;
    }
    $i++;
  }

  if ($form_state['values']['global'][EXTERNAL_ENVIRONMENT] <> $form['global'][EXTERNAL_ENVIRONMENT]['#default_value']) {
    variable_set(EXTERNAL_ENVIRONMENT, $form_state['values']['global'][EXTERNAL_ENVIRONMENT]);
  }
  variable_set(EXTERNAL_TRANSPORTS, $transport_config);
}

/**
 * Remove the last added transport from the configuration.
 */
function external_logger_config_form_remove_transport() {
  $transport_config = variable_get(EXTERNAL_TRANSPORTS);
  $size = count($transport_config) - 1;

  if (!$size > 0) {
    form_set_error(EXTERNAL_NAME, t('Cannot remove the last transport!'));
    return;
  }

  unset($transport_config[$size]);
  variable_set(EXTERNAL_TRANSPORTS, $transport_config);
}

/**
 * Add a new transport to the configuration.
 */
function external_logger_config_form_add_transport() {
  $transport_config = variable_get(EXTERNAL_TRANSPORTS);
  $transport_config[] = external_logger_get_default_transport_config();
  variable_set(EXTERNAL_TRANSPORTS, $transport_config);
}

/**
 * Reset the transport configuration, remove all transports, insert the default.
 */
function external_logger_config_form_reset_transport() {
  variable_set(EXTERNAL_TRANSPORTS, array(external_logger_get_default_transport_config()));
}

/**
 * The default configuration for the GELF transporters.
 *
 * @return array C
 *   Containing the default configuration for a transport.
 */
function external_logger_get_default_transport_config() {

  return array(
    EXTERNAL_NAME        => 'default-http',
    EXTERNAL_TYPE        => 0,
    EXTERNAL_IP          => 'your.domain.com',
    EXTERNAL_PORT        => '12201',
    EXTERNAL_USERNAME    => NULL,
    EXTERNAL_PASSWORD    => NULL,
    EXTERNAL_USE_SSL     => 0,
    EXTERNAL_SSC         => 0,
    EXTERNAL_VERIFY_PEER => 0,
  );
}
